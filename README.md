# Planet Earth Spectrum Allocation Scheme

This protocol is issued by and for the beings of Planet Earth in order to facilitate communication by means of the Electromagnetic Spectrum.

There are no owners of the Electromagnetic spectrum and its waves know no artificial boundaries. Its peculiar nature gives rise to problems of destructive interference, thereby restricting its use and giving it properties somewhat in line with those of a limited resource. But it is an invariably useful medium: capable of relaying information at the speed of, well... :| 

As such;

* preserving the inherent value of a clean, open electromagnetic spectrum;
* facilitating the freedom to speak openly or privately from anywhere on the planet;
* recognizing that a well-designed free market allocates limited resources efficiently;
* allowing that participating parties act only in self-interest;
* rejecting self-proclaimed owners and leasers of the electromagnetic spectrum as illegitimate rent-seekers;

the beings of planet earth establish the following protocol for designating communication channels in the electromagnetic spectrum.

## Overview

1. **Frequency bands (channels) in the Electromagnetic Spectrum are auctioned to the highest bidder (thereafter, *the provider*) at discrete time intervals in discrete geographical zones.** This ensures bands are being put to their most valued use.  It also ensures that a bidder with a more competitive model is not locked out of a channel indefinitely.

1. **The protocol recognizes the winner of an auction as the coordinator of wireless communication over that channel, in the given geographical area, for the duration of the specified interval.** This ensures that the channel remains clean of interference and incentivizes efficient communication service by the provider.

1. **Capital raised in each auction process is distributed to those using the channel for communication (*the clients*).** This incentivizes clients to adhere to the protocol instead of subscribing to a rogue transmitter. It also ensures that the value of the limited natural resource is distributed to the community in which that resource exists.

1. **The auction process is held in a decentralized smart contract.** This ensures that no central authority can be bribed or coerced into favoring an incumbant provider.  It also ensures that clients can independently verify that they're communicating with a legitimate provider.

1. **Payments for service on a channel are transferred from client to provider using a cryptographically secured payment channel**. This ensures that the client and the provider both carry proof of each transaction.  It also brings to trust to the contract without the need for a third party arbitrator or clearing-house.  It allows for persistence on the blockchain to occur infrequently.

1. **All transactions within this system (the auction and the disbursements) are denominated in PESA, an ERC20 token.** This allows transactions to be made electronically with no counterparty risk. It also provides a network bootstrapping mechanism, incentivizing participation before scale. 

## Definitions

## Auction Protocol

TBD

## Communication Protocol

1. Client looks up available channels for their current area on blockchain
1. Client chooses provider and opens payment channel with them.
1. Provider signs each return message with key used in the auction, proving they're the legitimate owner of that channel.
1. Client -> Provider frame has signature for payment, msgseqnum
1. Provider -> Client frame has signature for PESA rebate, msgseqnum
1. When either party decides to end the connection they can persist the payment channel to the blockchain, automatically settling the funds
1. Each data request comes with a payment open, (up to), provider "ACKs" payment

## Payment Channel

* payments from client to provider (signed by client)
* "ACKS" from provider to client (signed by provider)
* provider must not be able to claim payments without signing corresponding ACKs
* needs to be closeable by either party at any time

## Storage of ChannelSpaceTime Map in the Blockchain

I. Channel

* An integer which maps to some center-frequency of the band in question
* Channels should be pre-mapped (in line with standards that already exists today)

II. Space

* Theoretical -> some function that describes a continuous power over a three dimensional space relative to some point on earth
* v1 -> some (lat, long, alt) tuple which describes the "top-north-west" corner of a cube (vertex length of s) which has binary coverage (covered or not covered).
* (TODO: lookup a succinct way of storing geo-spacial data)

III. Time

* Expiration Date
* Assigned at the close of an auction, expiration\_date = close\_time + allocation\_time

IV. Provider

* Controlling address of the auction winner
* Address with which all communication frames are signed

## Rollout
* Channel by channel. Geo by geo. See 802.22, whitespace television channels experiments already underway in Scotland.
* It would be interesting to hold a single auction somewhere in the world.
* Likely not feasible in the United States unless the FCC gets onboard.  (And hey, they're all about deregulation right now amirite? -_-) 

## Open Problems (keep in mind many of these exist as problems today)
1. Squatting without providing service
    - there is a cost per time so this may be of no concern
    - one potential solution: clients could vote no-confidence which retriggers the auction?
    
1. What prevents non-bidders from broadcasting anyway?
    - one intention of redistributing the capital raised in the bidding is to incentivize clients to use the legitimate winner, thereby driving illegal broadcasters out of business.
    
1. Nothing in this scheme prevents a geographical monopoly across channels.
    - ie. if Telecom A can come close to coming up with the capital to win the auction on every channel, they have an incentive to pay a premium to do so.
    - naive solution would be to ban the same party from bidding on more than one channel or two contiguous channels or something. But in practice it is difficult to verify that two addresses are not just owned by the same real-world entity.
    
1. The FCC

1. How do we prevent the auction winner from operating many sock-puppet clients in order to dilute the redistribution pool and reclaim some of their winning bid?

## Open Questions
1. Do we generalize this protocol to all Electromagnetic transmission or keep it specific to wireless internet service providers (WISPs)?
1. Do we define geographical coverage as a function in four dimensions (3 spacial, 1 Power) or do we start with discrete spacial and a binary Power (a cube of space, covered or not)
1. We need a mechanism for a client to broadcast a "proof-of-reception". This is needed to allocate the redistribution to the client.  This would be useful in cases of client votes, and client "ratings" of WISPs.
1. We need some form of a bootstrapping protocol so that clients with no existing internet service can lookup existing service providers for the first time.
1. How do we design the auction?
1. Is this whole value-swap too good to be true?  We need to spend some time examining the incentives and make sure we're getting this right.
    - The operating costs of a rogue transmitter are lower, because they don't need to win the bid.  So they can lure clients away from the legitimate provider with lower data rates.
    - The legitimate provider uses the redistribution as an incentive for the client.
    - cost\_legitimate\_providers\_service - rebate >? cost\_illegitimate\_provider\_service  (ideally not, but probably)
    - This means we can't rely purely on these economics to disincentivize rogue transmitters.
    - (Though this problem already exists and is one reason the FCC exists)
1. Regulation of WISPs? Rating system by clients?
1. Are provider-rights transferrable?  Seems they'd have to be since we can't prevent that in the real world.  Then how do we make them transferrable on the blockchain?